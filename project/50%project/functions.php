<?php
function newConn($hostname, $user, $pswrd, $dbname)
{
    return new mysqli($hostname, $user, $pswrd, $dbname);
}

function array_push_assoc($array, $key, $value){
    $array[$key] = $value;
    return $array;
}

?>