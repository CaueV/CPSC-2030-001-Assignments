<?php include_once('p_header.php');?>
<?php include_once('connect.php');?>
<?php include_once('check.php');?>

<div class="p-container">
    <div class="welcome">
        Welcome back <?=$_SESSION['login_user'];?>
        <a href = "logout.php">Log Out</a>
    </div>
    <div class="p-list">
        <h2>Project List</h2>
        <ul>
        <?php
        $projs = $mysqli->query('call getProjs()');
        $c = 1;
        while ($proj = mysqli_fetch_assoc($projs)) {?>
            <li>
                <h3><?=$proj['pname']?></h3>
                <div class="arrow-down"></div>
                <div class="desc hidden">
                    <p><?=$proj['pdesc']?></p>
                </div>
            </li>
        <?php
        $c++;
        }
        ?>
        </ul>
    </div>
</div>
<?php include_once("p-footer.php");?>