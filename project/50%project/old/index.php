<?php include "header.php"?>
    <div class="banner bg-home">
        <div class="overlay">
            <div class="spacer"></div>
            <h1>We chage people's<br>lives</h1>
        </div>
    </div>
    <div class="container c1">
        <div class="txt">
            <h1 class="quote">"Generic enlightening quote"</h1>
            <p>
                Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed tempus urna et pharetra pharetra. Integer quis auctor elit sed vulputate mi. Sit amet risus nullam eget felis eget. Magna fringilla urna porttitor rhoncus dolor. Lorem sed risus ultricies tristique nulla. Magna ac placerat vestibulum lectus. At quis risus sed vulputate odio ut enim. Leo vel fringilla est ullamcorper eget nulla facilisi etiam. Consequat ac felis donec et odio pellentesque diam volutpat. Enim sit amet venenatis urna cursus eget nunc scelerisque viverra. Ullamcorper morbi tincidunt ornare massa eget egestas purus viverra accumsan.
            </p>
        </div>
        <img src="images/home-small-banner.jpg" alt="cientist lokking through that zoom thingy">
    </div>
    <div class="container c2">
        <h1>Some Heading</h1>
        <div class="img-list">
            <div class="item">
                <img src="images/home-img1.jpg" alt="woman in lab">
            </div>
            <div class="item">
                <img src="images/home-img2.jpg" alt="woman running">
            </div>
            <div class="item">
                <img src="images/home-img3.jpg" alt="old man">
            </div>
        </div>
        <div class="columns-2">
            <h2>Another heading</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Sed viverra ipsum nunc aliquet bibendum. Pretium nibh ipsum consequat nisl vel pretium lectus. Tortor at risus viverra adipiscing. Iaculis at erat pellentesque adipiscing. Egestas sed sed risus pretium quam vulputate dignissim suspendisse in. Eleifend mi in nulla posuere sollicitudin aliquam ultrices sagittis orci. Mollis nunc sed id semper risus in hendrerit. Proin fermentum leo vel orci porta. Sed ullamcorper morbi tincidunt ornare massa eget egestas purus. Egestas egestas fringilla phasellus faucibus scelerisque eleifend donec pretium vulputate. Eu scelerisque felis imperdiet proin fermentum leo vel. Consectetur libero id faucibus nisl tincidunt eget. Eu nisl nunc mi ipsum. Tortor id aliquet lectus proin nibh nisl condimentum id venenatis. Vestibulum lectus mauris ultrices eros in cursus turpis massa.</p>
            <p>Mattis nunc sed blandit libero volutpat sed. Malesuada fames ac turpis egestas integer eget aliquet nibh praesent. Facilisi morbi tempus iaculis urna id. Amet nisl purus in mollis nunc sed id. Blandit turpis cursus in hac habitasse platea dictumst quisque sagittis. Varius morbi enim nunc faucibus a pellentesque sit amet porttitor. Scelerisque viverra mauris in aliquam sem fringilla ut morbi tincidunt. Sagittis aliquam malesuada bibendum arcu vitae elementum. Neque ornare aenean euismod elementum nisi quis eleifend quam. Felis donec et odio pellentesque diam. Nisl suscipit adipiscing bibendum est ultricies integer quis auctor. Varius morbi enim nunc faucibus a pellentesque sit. Maecenas ultricies mi eget mauris. Sit amet purus gravida quis blandit turpis cursus in hac.</p>
        </div>
    </div>
<?php include "footer.php"?>