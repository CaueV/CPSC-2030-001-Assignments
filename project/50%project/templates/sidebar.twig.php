<div class="side-bar">
    <h2>{{ side_title }}</h2>
    <ul>
        {% for item in list %}  
        <li><a href="{{home_url}}?name={{ item.name }}&form={{ item.form }}">{{ item.name }}{% if item.form!='-----' %} - {{ item.form }}{% endif %}</a></li>
        {% endfor %}
    </ul>
</div>