$(document).ready(function(){
    $(".succ").hide();

    //shows description preview when clicked
    $(".arrow-down").click(function() {
        $( this ).next().toggleClass("hidden");
        $( this ).toggleClass("dark");
    });

    //adds new field for goal
    $("#add-field").click(function(){
        $(".goal-list").append('<input type="text" class="goal-item" required>');
    });

    //stops form from refreshing page
    //reads the values for the new project
    //makes an ajax request to update the database and displays a success message 
    $("#p-form").submit(function(e){
        e.preventDefault();

        let pname = $("#p-name").val();
        let pdesc = $("#p-desc").val();
        let id;

        let goalsList = $(".goal-item");
        console.log(goalsList);
        let goals = Array();

        goalsList.each(function(){
            goals.push($(this).val());
        });

        $.post("functions.php",{
            action: 'add-project',
            pname: pname,
            pdesc: pdesc,
            goals: goals
        },function(){
            $(".succ").text("Your project has been submitted successfully");
            $(".succ").show();
        });
    });

    //sends a request to delete the related entries from the database
    $("#minus").click(function(){
        if (confirm("Are you sure you want to delete this project?")) {
            let pid = $("#minus").attr("pid");
            console.log(pid);

        $.post("functions.php",{
            action: 'del-project',
            pid: pid
        },function(){
            $(".succ").text("Your project has been successfully deleted");
            $(".succ").show();
        });
        }else{
        }
    });
});
