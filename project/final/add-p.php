<?php include_once('p-header.php');?>
<?php include_once('functions.php');?>
<?php include_once('check.php');?>
<div class="back"><a href="projects.php">Go back</a> to project list</div>
<div class="p-list">
    <form action="" id="p-form">
        <div class="form-item">
            <label for="p-name">Project Name: </label><input type="text" name="p-name" id="p-name" required>
        </div>
        <div class="form-item">
            <label for="p-desc">Project Description: </label><br><textarea rows="4" cols="50" name="p-desc" id="p-desc" required></textarea>
        </div>
        <div class="form-item">
            <label>Form Goals: </label><input type="button" value="Add Field" id="add-field">
            <div class="goal-list" id="goal-list">
                <input type="text" class="goal-item" required>
            </div>
        </div>
        <input type="submit" value="Add Project">
    </form>
    <div class="succ"></div>
</div>
<?php include_once("p-footer.php");?>