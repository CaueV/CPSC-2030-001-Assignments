<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Biocyte Pharmaceuticals</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
</head>
<body>
    <nav>
        <div class="logo-area">
            <img id="logo" src="images/logo.png" alt="Leaf Logo">
        </div>
        <ul class="nav-bar">
            <li class="item"><a href="index.php">Home</a>
        </li><li class="item">
                <a href="products.php">Products</a>
            </li><li class="item">
                    <a href="about.php">About Us</a>
                </li>
        </ul>
        <div class="login">
            <a href="login.php">Log In</a>
        </div>
    </nav>
