<?php include_once('header.php')?>
<?php include_once('functions.php');?>
<div class="banner bg-prod">
    <div class="overlay">
        <div class="spacer"></div>
        <h1>Products you can<br>trust</h1>
    </div>
</div>
<div class="line divisor"></div>
<div class="container">
    <ul class="product-list">
    <?php
    $meds = $mysqli->query('call getMeds()');
    while ($med = mysqli_fetch_assoc($meds)) {?>
        <li>
        <img src="images/<?=$med['img']?>" alt="<?=$med['name']?>">
        <div class="desc">
            <h2><?=$med['name']?></h2>
            <p><?=$med['description']?></p>
        </div>
    </li>
    <?php
    }
    ?>
    </ul>
</div>
<?php include_once("footer.php")?>