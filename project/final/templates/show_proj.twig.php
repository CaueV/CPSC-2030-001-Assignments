<div class="project-items">
    <h2>Project: {{pj.pname}}</h2>
    <div class="header">Project Description</div>
    <p class="p-description">{{pj.pdesc}}</p>
    <div class="header">Goals</div>
    <div class="p-description">
        {% for goal in goals %}
            <li class="item">{{goal}}</li>
        {%endfor%}
    </div>
    <div class="p-del">
        <div>Delete Project<img id="minus" class="minus" src="images/minus.ico" alt="minus sign" pid="{{ pj.pid }}"></div>
        <div class="succ"></div>
    </div>
</div>
      