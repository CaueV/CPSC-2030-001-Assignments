<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{page_title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script src="javascript.js"></script>
</head>
<body>
    <div class="p-header">
        <h1><a href="index.php">Biocyte Farmaceuticals</a></h1>
        <img id="logo" src="images/logo.png" alt="Leaf Logo">
    </div>
    <div class="p-menu">
        {% include 'title.twig.php'%}
    </div>
    <div>
        {% include 'show_proj.twig.php'%}
    </div>
</body>      
</html>