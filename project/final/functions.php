<?php
include_once('connect.php');

function newConn($hostname, $user, $pswrd, $dbname){
    return new mysqli($hostname, $user, $pswrd, $dbname);
}

function array_push_assoc($array, $key, $value){
    $array[$key] = $value;
    return $array;
}

//switch that will call a function depending on ajax requests
if(isset($_POST['action'])){
    switch($_POST['action']){
        case 'add-project':
            addProject($hostname, $user, $pswrd, $dbname);
            break;
        case 'del-project':
            delProject($hostname, $user, $pswrd, $dbname);
            break;
    }
}

//adds a project entry to all tables
function addProject($hostname, $user, $pswrd, $dbname){
    $pname = $_POST['pname'];
    $pdesc = $_POST['pdesc'];
    $goals = $_POST['goals'];

    $addprojcon = newConn($hostname, $user, $pswrd, $dbname);
    $addprojcon->query("call addProj('$pname','$pdesc')");

    $getidcon = newConn($hostname, $user, $pswrd, $dbname);
    $id = mysqli_fetch_assoc($getidcon->query("call getId('$pname')"))['pid'];

    $addgoalscon = newConn($hostname, $user, $pswrd, $dbname);
   foreach ($goals as $goal) {
    $addgoalscon->query("call addGoal('$id','$goal')");
   }
}

//deletes project entries from all tables
function delProject($hostname, $user, $pswrd, $dbname){
    $pid = $_POST['pid'];

    $delprojcon = newConn($hostname, $user, $pswrd, $dbname);

    $delprojcon->query("call delProj('$pid')");
    $delprojcon->query("call delGoal('$pid')");
}