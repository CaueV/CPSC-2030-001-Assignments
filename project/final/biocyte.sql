-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: 03-Dez-2018 às 07:03
-- Versão do servidor: 10.1.36-MariaDB
-- versão do PHP: 7.2.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `biocyte`
--

DELIMITER $$
--
-- Procedures
--
CREATE DEFINER=`root`@`localhost` PROCEDURE `addGoal` (IN `id` INT, IN `t` TEXT)  NO SQL
INSERT INTO goals(pid,goal) values(id,t)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `addProj` (IN `n` VARCHAR(20), IN `d` TEXT)  NO SQL
INSERT INTO projects (pname,pdesc) values(n,d)$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delGoal` (IN `id` INT)  NO SQL
DELETE FROM goals WHERE goals.pid=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `delProj` (IN `id` INT)  NO SQL
DELETE from projects where projects.pid=id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getGoals` (IN `id` INT)  NO SQL
SELECT * from goals where goals.pid = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getId` (IN `name` VARCHAR(20))  NO SQL
SELECT pid from projects where projects.pname = name$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getMeds` ()  NO SQL
SELECT * FROM products$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProj` (IN `id` INT)  NO SQL
SELECT * from projects where projects.pid = id$$

CREATE DEFINER=`root`@`localhost` PROCEDURE `getProjs` ()  NO SQL
Select * from projects$$

DELIMITER ;

-- --------------------------------------------------------

--
-- Estrutura da tabela `goals`
--

CREATE TABLE `goals` (
  `pid` int(11) NOT NULL,
  `goal` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `goals`
--

INSERT INTO `goals` (`pid`, `goal`) VALUES
(3, 'Add a new DLC'),
(3, 'Make shovel knight the number 1 most downloaded game on the nintendo switch'),
(3, 'Sell amiibos like there was no tomorrow'),
(4, 'Get rid of the boring weapons'),
(4, 'Make cells more easily available'),
(5, 'People need to be more sad when playing this game'),
(5, 'Make a paid dlc called \"Trip\"'),
(6, 'Add more amazing songs as soundtrack'),
(6, 'Become the number one e-sports'),
(6, 'Demolish riot games headquarters');

-- --------------------------------------------------------

--
-- Estrutura da tabela `products`
--

CREATE TABLE `products` (
  `item_num` int(11) NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` text NOT NULL,
  `img` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `products`
--

INSERT INTO `products` (`item_num`, `name`, `description`, `img`) VALUES
(1, 'Soul Catcher', 'Increases the amount of SOUL gained when striking an enemy with the nail.', 'Soul_Catcher.png'),
(2, 'Thorns of Agony', 'When taking damage, sprout thorny vines that damage nearby foes.', 'Thorns_of_Agony.png'),
(3, 'Flukenest', 'Transforms the Vengeful Spirit spell into a horde of volatile baby flukes.', 'Flukenest.png');

-- --------------------------------------------------------

--
-- Estrutura da tabela `projects`
--

CREATE TABLE `projects` (
  `pid` int(10) NOT NULL,
  `pname` varchar(20) NOT NULL,
  `pdesc` text NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `projects`
--

INSERT INTO `projects` (`pid`, `pname`, `pdesc`) VALUES
(3, 'Shovel Knight', 'Shovel Knight: Treasure Trove is the full and complete edition of Shovel Knight, a sweeping classic action adventure game series with awesome gameplay, memorable characters, and an 8-bit retro aesthetic! Become Shovel Knight, wielder of the Shovel Blade, as he runs, jumps, and battles in a quest for his lost beloved. Take down the nefarious knights of the Order of No Quarter and their menacing leader, The Enchantress.\r\n\r\nBut that\'s not everything! Shovel Knight: Treasure Trove will feature three additional campaigns that are games all unto themselves! Take control of Plague Knight, and Specter Knight on adventures of their own, with King Knight joining later as a free update! Together, they form a grand and sweeping saga!\r\n\r\nWith a fully cooperative campaign, a full-featured challenge mode, body swap mode, and a 4 player battle mode (Coming soon!) you\'ll be digging for a long time. With Shovel Knight: Treasure Trove, you get it all. Uphold the virtues of Shovelry, earn relics and riches, and discover the true meaning of shovel justice!'),
(4, 'Dead Cells', 'Dead Cells is a rogue-lite, Castlevania-inspired action-platformer, allowing you to explore a sprawling, ever-changing castle... assuming you\'re able to fight your way past its keepers.\r\n\r\nTo beat the game, you\'ll have to master 2D \"souls-lite combat\" with the ever-present threat of permadeath looming. No checkpoints. Kill, die, learn, repeat.'),
(5, 'Journey', 'Explore the ancient, mysterious world of Journey as you soar above ruins and glide across sands to discover its secrets. Play alone or in the company of a fellow traveler and explore its vast world together. Featuring stunning visuals and a Grammy-nominated musical score, Journey delivers a breathtaking experience like no other.'),
(6, 'Rocket League', 'Winner or Nominee of more than 150 \'Best of 2015\' Awards, including Game of the Year, Best Sports Game, and Best Multiplayer Game, Rocket League combines soccer with driving in an unbelievable physics-based multiplayer-focused sequel to Supersonic Acrobatic Rocket-Powered Battle-Cars! Choose from a variety of high-flying vehicles equipped with huge rocket boosters to score amazing aerial goals and pull off incredible, game-changing saves!');

-- --------------------------------------------------------

--
-- Estrutura da tabela `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `uname` varchar(20) NOT NULL,
  `pswd` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Extraindo dados da tabela `users`
--

INSERT INTO `users` (`id`, `uname`, `pswd`) VALUES
(1, 'Captain_Falcon', '4988cee8c71f8957aa768bc1f47c46de'),
(2, 'Waluigi', '9e44635fbd9b420956fa998ed7c81f17'),
(3, 'Ash_Ketchum', '21acd8ca94af600dbf3f167e101bef8a');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`item_num`),
  ADD KEY `item_num` (`item_num`);

--
-- Indexes for table `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`pid`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `item_num` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `projects`
--
ALTER TABLE `projects`
  MODIFY `pid` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
