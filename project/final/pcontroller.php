<?php
require_once 'vendor\autoload.php';
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

include("functions.php");

if(isset($_GET['pid'])){
    $pid = $_GET['pid'];

    $aProj = $mysqli->query("call getProj($pid)");
    $pj = mysqli_fetch_assoc($aProj);

    $anotherConn = newConn($hostname, $user, $pswrd, $dbname);
    $projGoals = $anotherConn->query("call getGoals($pid)");

    $goals = array();
    while($row = mysqli_fetch_assoc($projGoals)){
        array_push($goals,$row['goal']);
    }

    $projArray = array(
        'this_page' => 'pcontroller.php',
        'pj' => $pj,
        'goals' => $goals
    );
    
    echo $twig->render('proj-desc.twig.php', $projArray);
}else{
    header("Location: projects.php");
}