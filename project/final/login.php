<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Biocyte Pharmaceuticals</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
<?php
   include("connect.php");
   session_start();
   
   if(isset($_SESSION['login_user'])){
    header("location: projects.php");
   }

   if($_SERVER["REQUEST_METHOD"] == "POST") {      
      $uname = $mysqli->real_escape_string($_POST['login']);
      $pswd = md5($mysqli->real_escape_string($_POST['pswd']));
      
      $sql = "SELECT id FROM users WHERE uname = '$uname' and pswd = '$pswd'";
      $result = $mysqli->query($sql);
      $row = mysqli_fetch_assoc($result);
      
      $count = mysqli_num_rows($result);
		
      if($count == 1) {
         $_SESSION['login_user'] = $uname;
         header("location: projects.php");
      }else {
         $error = "Your Login Name or Password is invalid";
      }
   }
?>
    <div class="container">
        <div class="login-box">
        <p><a href="index.php">Go Back</a> to Biocyte website</p>
        <form method="post" action="" id="formlogin" name="formlogin">
            <input type="text" name="login" id="login"  placeholder="User Name"/>
            <br>
            <input type="password" name="pswd" id="pswd" placeholder="Password"/>
            <br>
            <input type="submit" value="Log In"/>
            </form>
        </div>
    </div>
</body>
</html>