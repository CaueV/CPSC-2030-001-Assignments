<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Biocyte Pharmaceuticals</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
    <div class="container">
        <div class="login-box">
        <form method="post" action="/check.php" id="formlogin" name="formlogin">
            <input type="text" name="login" id="login"  placeholder="User Name"/>
            <br>
            <input type="password" name="pswd" id="pswd" placeholder="Password"/>
            <br>
            <input type="submit" value="Log In"/>
            </form>
        </div>
    </div>
</body>
</html>