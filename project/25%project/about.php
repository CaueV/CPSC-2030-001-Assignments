<?php include "header.php"?>
<div class="banner bg-about">
    <div class="overlay">
        <div class="spacer"></div>
        <h1>Biocyte has a<br>rich history</h1>
    </div>
</div>
<div class="container">
    <div class="about">
        <h2>About Us</h2>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Potenti nullam ac tortor vitae purus. Sit amet justo donec enim diam vulputate. Mi eget mauris pharetra et. Nec tincidunt praesent semper feugiat nibh sed pulvinar proin. Praesent tristique magna sit amet purus gravida quis. Nibh sit amet commodo nulla facilisi. Lorem donec massa sapien faucibus et molestie. Lacus sed viverra tellus in hac habitasse platea. Ac auctor augue mauris augue neque gravida. Sed blandit libero volutpat sed. Egestas erat imperdiet sed euismod nisi. Viverra accumsan in nisl nisi scelerisque eu ultrices vitae. In ornare quam viverra orci. Dictum sit amet justo donec enim diam vulputate. Ipsum suspendisse ultrices gravida dictum fusce ut placerat orci nulla. Aenean pharetra magna ac placerat vestibulum lectus.</p>
        <p>Volutpat odio facilisis mauris sit amet massa vitae. Praesent elementum facilisis leo vel fringilla est ullamcorper. Et magnis dis parturient montes nascetur ridiculus mus. Pretium lectus quam id leo in vitae turpis massa. Tempus imperdiet nulla malesuada pellentesque. Habitant morbi tristique senectus et netus et. Dui ut ornare lectus sit amet est placerat in. Id nibh tortor id aliquet lectus proin nibh nisl. Enim neque volutpat ac tincidunt vitae semper quis lectus nulla. Sollicitudin ac orci phasellus egestas tellus rutrum. Ipsum dolor sit amet consectetur adipiscing elit. Fermentum dui faucibus in ornare quam viverra orci. Iaculis nunc sed augue lacus viverra vitae. Egestas pretium aenean pharetra magna ac placerat vestibulum lectus mauris. Nam at lectus urna duis. Quam id leo in vitae turpis massa. Et tortor at risus viverra. Risus in hendrerit gravida rutrum quisque non tellus orci ac.</p>
        <img src="images/about-desc.jpg" alt="building">
        <p>Eu augue ut lectus arcu bibendum at. Vitae justo eget magna fermentum iaculis eu non diam phasellus. Quam quisque id diam vel quam elementum pulvinar etiam. Faucibus in ornare quam viverra orci sagittis eu. Ipsum a arcu cursus vitae congue. Eget aliquet nibh praesent tristique magna sit amet purus gravida. Sociis natoque penatibus et magnis. Viverra tellus in hac habitasse platea dictumst vestibulum rhoncus. Feugiat nibh sed pulvinar proin gravida hendrerit. Turpis cursus in hac habitasse platea. Consequat interdum varius sit amet mattis vulputate enim. Ultrices sagittis orci a scelerisque. Eu turpis egestas pretium aenean pharetra magna ac. Id volutpat lacus laoreet non curabitur gravida. Pellentesque pulvinar pellentesque habitant morbi tristique. Mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien et.</p>
    </div>
</div>
<?php include "footer.php"?>