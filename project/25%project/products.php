<?php include "header.php"?>
<div class="banner bg-prod">
    <div class="overlay">
        <div class="spacer"></div>
        <h1>Products you can<br>trust</h1>
    </div>
</div>
<div class="line divisor"></div>
<div class="container">
    <ul class="product-list">
        <li>
            <img src="images/r1.jpg" alt="remedy 1">
            <div class="desc">
                <h2>Med 1 Name</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nullam vehicula ipsum a arcu. Viverra aliquet eget sit amet tellus cras adipiscing.</p>
            </div>
            
        </li>
        <li>
            <img src="images/r2.jpg" alt="remedy 2">
            <div class="desc">
                <h2>Med 2 Name</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nullam vehicula ipsum a arcu. Viverra aliquet eget sit amet tellus cras adipiscing.</p>            </div>
        </li>
        <li>
            <img src="images/r3.jpg" alt="remedy 3">
            <div class="desc">
                <h2>Med 3 Name</h2>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Nullam vehicula ipsum a arcu. Viverra aliquet eget sit amet tellus cras adipiscing.</p>            </div>
        </li>
    </ul>
</div>
<?php include "footer.php"?>