1.	SELECT name from pokemon ORDER BY speed LIMIT 10;
2.	select DISTINCT name,form from (
		select * from (
			select name,form,type1,type2 from (
				select name,form,type1,type2 from (
					select name,form,type1,type2 from pokemon as p 
					INNER JOIN 
					(SELECT defending FROM type_eff_def where ground=2) as seg
					on p.type1=seg.defending or p.type2=seg.defending
				) as t1 
				INNER JOIN 
				(SELECT defending FROM type_eff_def where steel=0.5) as nves
				on t1.type1=nves.defending or t1.type2=nves.defending
			) as t2 
			LEFT JOIN 
			(SELECT defending FROM type_eff_def where ground=0.5) as nveg 
			on t2.type1=nveg.defending or t2.type2=nveg.defending where nveg.defending is null
		) as t3 
		LEFT JOIN 
		(SELECT defending FROM type_eff_def where steel=2) as ses 
		on t3.type1=ses.defending or t3.type2=ses.defending where ses.defending is null
	) as t4
This query makes sure that all pokemon listed are both vulnerable to ground and resistant to steel. It makes sure that there are no
instances where type 1 might be vulnerable to ground and type 2 is resistant to ground, making the damage neutral.
Forms were kept because some change the type of the pokemon.

3.	select distinct name,form from(
    		select * from(
   			select name,form,type1,type2 from (
        			select name,form,type1,type2 from pokemon where total BETWEEN 200 AND 500
    			) as p
    			INNER JOIN 
			(SELECT defending FROM type_eff_def where water=2) as sew
			on p.type1=sew.defending or p.type2=sew.defending
		) as t1
    		LEFT JOIN 
		(SELECT defending FROM type_eff_def where water=0.5) as nvew 
		on t1.type1=nvew.defending or t1.type2=nvew.defending where nvew.defending is null
	) as t2
4.	select distinct name from(
    		select * from(
   			select name,type1,type2 from (
        			select name,type1,type2 from pokemon where 			
                		form="mega" ORDER by atk desc
    			) as p
    			INNER JOIN 
			(SELECT defending FROM type_eff_def where fire=2) as sef
			on p.type1=sef.defending or p.type2=sef.defending
		) as t1
    		LEFT JOIN 
		(SELECT defending FROM type_eff_def where fire=0.5) as nvef 
		on t1.type1=nvef.defending or t1.type2=nvef.defending where nvef.defending is null
	) as t2