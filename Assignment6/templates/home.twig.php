<!DOCTYPE html>
<html lang="en">
<script>    
    if(typeof window.history.pushState == 'function') {
        window.history.pushState({}, "Hide", "{{this_page}}");
    }
</script>
<head>
    <meta charset="utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>{{page_title}}</title>
    <link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet/less" type="text/css" media="screen" href="styles.less" />
    <script src="https://cdnjs.cloudflare.com/ajax/libs/less.js/3.7.1/less.min.js"></script>
</head>
<body>
    <div class="grid-container">
        <div class="title-a">
            {% include 'title.twig.php'%}
        </div>
        <div class="side-a">
            {% include 'sidebar.twig.php'%}
        </div>
        <div class="fav-a">
            {% include 'poke_card.twig.php'%}
        </div>
        <div class="table-a">
            {% include 'poke_table.twig.php'%}
        </div>  
    </div>
</body>      
</html>