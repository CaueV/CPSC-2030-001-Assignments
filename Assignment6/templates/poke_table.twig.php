<div class="container">
    <table class="poke-table">
        <tr>
            <th>dex #</th>
            <th>Name</th>
            <th>Form</th>
            <th>Type 1</th>
            <th>Type 2</th>
        </tr>
        {% for poke in pokes %}
            <tr>
                <td>{{poke.num}}</td>
                <td><a href="{{home_url}}?name={{ poke.name }}&form={{ poke.form }}">{{poke.name}}</a></td>
                <td>{{poke.form}}</td>
                <td>{{poke.type1}}</td>
                <td>{{poke.type2}}</td>
            </tr>
        {%endfor%}
    </table>
</div>
      