<div class="fav-bar">
<h2>{{favs_title}}</h2>
    {% if favs is empty %}
        <div class="empty">Click on a Pokemon to add it to your favorites list</div>
    {% endif %}
    {% for fav in favs %}
        <div class="card">
            <img src="sprites/{% if fav.form == '-----' or fav.form == 'Normal' or fav.form == 'Mega' %}{{ fav.num }}{% else %}{{ fav.num }}-{{ fav.form }}{% endif %}.png" alt="{{ fav.name }}'s sprite'">
            <a href="{{ this_page }}?name={{ fav.name }}&form={{ fav.form }}">
                <h2>
                    #{{ fav.num }} - {{ fav.name }}
                    {% if fav.form != '-----' %}
                        - {{ fav.form }}
                    {% endif %}
                </h2>
            </a>
            <div class="types">
                {% if fav.type2 == '-----' or fav.form == 'Normal' or fav.form == 'Mega' %}
                    {{ fav.type1 }}
                {% else %}
                {{ fav.type1 }}, {{ fav.type2 }}
                {% endif %}
            </div>
        </div>
    {%endfor%} 
</div>
