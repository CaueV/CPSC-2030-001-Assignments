<?php
session_start();
require_once '.\vendor\autoload.php';
$loader = new Twig_Loader_Filesystem('templates');
$twig = new Twig_Environment($loader);

//connects to the database and adds helper functions
include_once("connect.php");
include_once("functions.php");

//gets all pokemon from database and adds them to an array
//all different forms and megas will have their own line, because they are technically different pokemon
$all_poke = $mysqli->query('call getAll()');
$pokes = array();
while($row = mysqli_fetch_assoc($all_poke)){
    $aPoke['num'] = $row['dex#'];
    $aPoke['name'] = $row['name'];
    $aPoke['form'] = $row['form'];
    $aPoke['type1'] = $row['type1'];
    $aPoke['type2'] = $row['type2'];
    array_push($pokes,$aPoke);
}

//this array will be used in the session
$favs = array();

if (isset($_SESSION['array'])){
    $favs = $_SESSION['array'];
    //No time to die is set. User will have his favorite pokemon list saved for as long as the server allows
}

//name and form will be used for most queries
if(isset($_GET['name']) && isset($_GET['form'])){
    $aName = $_GET['name'];
    $aName = $mysqli->real_escape_string($aName);
    $aForm = $_GET['form'];
    $aForm = $mysqli->real_escape_string($aForm);
    $nameForm = $aName.'-'.$aForm;
}

//adds pokemon to favorites array or removes it if already there
if(isset($_GET['name']) && isset($_GET['form'])){
    if (array_key_exists($nameForm, $favs)) {
        unset($favs[$nameForm]);
    }else if(sizeof($favs) < 7){
        $newConn = newConn($hostname, $user, $pswrd, $dbname);
        $favPoke = $newConn->query("call getOneFav('$aName','$aForm')");
        $myFavPoke = mysqli_fetch_assoc($favPoke); 
        $aPoke['num'] = $myFavPoke['dex#'];
        $aPoke['name'] = $myFavPoke['name'];
        $aPoke['form'] = $myFavPoke['form'];
        $aPoke['type1'] = $myFavPoke['type1'];
        $aPoke['type2'] = $myFavPoke['type2'];
        $favs += array($myFavPoke['name'].'-'.$myFavPoke['form'] => $aPoke);
    }
}

$_SESSION['array'] = $favs;

//generates the list of most popular pokemon. The max number of pokemon on the list is set
//by the $max variable
$anotherConn = newConn($hostname, $user, $pswrd, $dbname);
$mostPop = $anotherConn->query("call getMostPopular()");
$count = 0;
$list = array();
$max = 6;
while($item = mysqli_fetch_assoc($mostPop) and $count< $max){
    $coolPoke['name'] = $item['name'];
    $coolPoke['form'] = $item['form'];
    $list += array($coolPoke['name'].'-'.$coolPoke['form'] => $coolPoke);
    $count++;
}

//renders the page
echo $twig->render('home.twig.php', array(
    'page_title' => 'Pokedex',
    'this_page' => 'controller.php',
    'img_src' => 'logo.png',
    'pokes' => $pokes,
    'favs_title' => 'My Favorite Pokemon',
    'favs' => $favs,
    'side_title' => 'Most Popular Pokemon',
    'list' => $list
));
?>