//selects the button and toggles all animates classes
$(".menu-btn").click(function() {
    $(this).toggleClass("rotate");
    $('.item-list > div').toggleClass('in-out');
    $('#bullet').toggleClass("in-bullet");
    $('#bullet').toggleClass("out-bullet");
});

//select all menu items
$('.item-list > div').click(function(e) {
    e.preventDefault();
    let $self = $(this);
    //instead of toggling a class I used the animate function to try and make the link redirect only after the animation
    //however it was still triggering too son, so I worked around it using setTimeout
    //and since all the animate colde was already written I didn't want to go back to the class togling ways
    
    //PS:I used 2 separate animate functions so I could set different durations and easing so it would look a bit more like a 
    //fired bullet
    $('#bullet').animate({
        left:$self.css("left"),
    },{
        duration:50,
        easing: "linear"
    }).animate({
        top:$self.css("top")
    },{
        queue:false,
        duration:300,
        complete: function(){
            setTimeout(function(){
                document.location = $self.children("a").attr('href');
            },1500);    
        }
    })
});