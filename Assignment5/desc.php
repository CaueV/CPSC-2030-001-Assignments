<?php include_once("header.php");?>
<?php include_once("connect.php");?>
<?php include_once("functions.php");?>
<?php
$name = $_GET['name'];
$name = $mysqli->real_escape_string($name);
$q1 = "call getStrong()";
$q2 = "call getWeak()";
$q3 = "call getResTo()";
$q4 = "call getResIt()";

$a_poke = $mysqli->query("call getOne('$name')");
$row = mysqli_fetch_assoc($a_poke);
while($row && $row['form']!='Mega'){?>
    <div class="grid">
        <div class="info">
            <h2>#<?=$row['dex#']?> - <?=$row['name']?></h2>
            <div class="types">
                <?php 
                if( $row['type2'] == '-----'){
                    $t1 = $row['type1'];
                    $numtypes = 1;?>
                    <a href="index.php?type=<?=$row['type1']?>"><?=$row['type1']?></a>
                <?php
                } else {
                    $t1 = $row['type1'];
                    $t2 = $row['type2'];
                    $numtypes = 2;?>
                    <a href="index.php?type=<?=$row['type1']?>"><?=$row['type1']?></a>, <a href="index.php?type=<?=$row['type2']?>"><?=$row['type2']?></a>
                <?php
                }
                ?>
            </div>
            <table class="stats">
                <tr>
                    <th>HP</th>
                    <th>ATACK</th>
                    <th>DEFENSE</th>
                    <th>SP.ATK</th>
                    <th>SP.DEF</th>
                    <th>SPEED</th>
                    <th>TOTAL</th>
                </tr>
                <tr>
                    <td><?=$row['hp']?></td>
                    <td><?=$row['atk']?></td>
                    <td><?=$row['def']?></td>
                    <td><?=$row['sp.atk']?></td>
                    <td><?=$row['sp.def']?></td>
                    <td><?=$row['speed']?></td>
                    <td><?=$row['total']?></td>
                </tr>
            </table>
            <h3>Weak to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $weakTo = $newConn->query("call getWeakToOne('$t1')");
                break;
                case 2:
                $weakTo = $newConn->query("call getWeakToTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($weakTo) > 0){
                $wt = mysqli_fetch_assoc($weakTo);
                echo  $wt['attaking'];
                while($wt = mysqli_fetch_assoc($weakTo)){
                    echo  ", ".$wt['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
            <h3>Immune to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $imTo = $newConn->query("call getImmuneOne('$t1')");
                break;
                case 2:
                $imTo = $newConn->query("call getImmuneTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($imTo) > 0){
                $it = mysqli_fetch_assoc($imTo);
                echo  $it['attaking'];
                while($it = mysqli_fetch_assoc($imTo)){
                    echo  ", ".$it['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
            <h3>Resistant to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $resTo = $newConn->query("call getResToOne('$t1')");
                break;
                case 2:
                $resTo = $newConn->query("call getResToTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($resTo) > 0){
                $rt = mysqli_fetch_assoc($resTo);
                echo  $rt['attaking'];
                while($rt = mysqli_fetch_assoc($resTo)){
                    echo  ", ".$rt['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
        </div>
        <div class="sprite">
        <img src="sprites/<?php
            if ( $row['form'] == '-----' || $row['form'] == 'Normal'|| $row['form'] == 'Mega'){
                echo $row['dex#'];
            }else{
                echo  $row['dex#']."-".$row['form'];
            }
            $row['dex#']
            ?>.png" alt="<?=$row['name']?>'s sprite'">
        </div>
    </div>
    <?php
    $row = mysqli_fetch_assoc($a_poke);
}
while($row){?>
    <div class="grid">
        <div class="info">
            <h2>#<?=$row['dex#']?> - <?=$row['name']?> - <?=$row['form']?></h2>
            <div class="types">
                <?php 
                if( $row['type2'] == '-----'){?>
                    <a href="index.php?type=<?=$row['type1']?>"><?=$row['type1']?></a>
                <?php
                } else {?>
                    <a href="index.php?type=<?=$row['type1']?>"><?=$row['type1']?></a>, <a href="index.php?type=<?=$row['type2']?>"><?=$row['type2']?></a>
                <?php
                }
                ?>
            </div>
            <table class="stats">
                <tr>
                    <th>HP</th>
                    <th>ATACK</th>
                    <th>DEFENSE</th>
                    <th>SP.ATK</th>
                    <th>SP.DEF</th>
                    <th>SPEED</th>
                    <th>TOTAL</th>
                </tr>
                <tr>
                    <td><?=$row['hp']?></td>
                    <td><?=$row['atk']?></td>
                    <td><?=$row['def']?></td>
                    <td><?=$row['sp.atk']?></td>
                    <td><?=$row['sp.def']?></td>
                    <td><?=$row['speed']?></td>
                    <td><?=$row['total']?></td>
                </tr>
            </table>
            <h3>Weak to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $weakTo = $newConn->query("call getWeakToOne('$t1')");
                break;
                case 2:
                $weakTo = $newConn->query("call getWeakToTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($weakTo) > 0){
                $wt = mysqli_fetch_assoc($weakTo);
                echo  $wt['attaking'];
                while($wt = mysqli_fetch_assoc($weakTo)){
                    echo  ", ".$wt['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
            <h3>Immune to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $imTo = $newConn->query("call getImmuneOne('$t1')");
                break;
                case 2:
                $imTo = $newConn->query("call getImmuneTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($imTo) > 0){
                $it = mysqli_fetch_assoc($imTo);
                echo  $it['attaking'];
                while($it = mysqli_fetch_assoc($imTo)){
                    echo  ", ".$it['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
            <h3>Resistant to:</h3>
            <?php 
            $newConn = newConn($hostname, $user, $pswrd, $dbname);
            switch($numtypes){
                case 1:
                $resTo = $newConn->query("call getResToOne('$t1')");
                break;
                case 2:
                $resTo = $newConn->query("call getResToTwo('$t1','$t2')");
                break;
            }
            if(mysqli_num_rows($resTo) > 0){
                $rt = mysqli_fetch_assoc($resTo);
                echo  $rt['attaking'];
                while($rt = mysqli_fetch_assoc($resTo)){
                    echo  ", ".$rt['attaking'];
                }
            }else{
                echo "none";
            }
            ?>
        </div>
        <div class="sprite">
        <img src="sprites/<?php
            if ( $row['form'] == '-----' || $row['form'] == 'Normal'|| $row['form'] == 'Mega'){
                echo $row['dex#'];
            }else{
                echo  $row['dex#']."-".$row['form'];
            }
            $row['dex#']
            ?>.png" alt="<?=$row['name']?>'s sprite'">
        </div>
    </div>
    <?php
    $row = mysqli_fetch_assoc($a_poke);
}?>
<?php include_once("footer.php");?>