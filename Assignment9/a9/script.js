$(document).ready(function(){
    //When the user clicks on the send button it sends an ajax request to the server to store this information
    $('#form').on('submit', function(e) {
        e.preventDefault();	
        let user = $("#uname").val();
        let clientmsg = $("#usermsg").val();
        let now = new Date($.now());
        let timenow = now.toTimeString().split(' ')[0].substr(0, 5);
        $.post('server.php',{
            action:'insert',
            name: user,
            time: timenow,
            text: clientmsg
        });	
		$("#usermsg").val("");
    });
    
    $(document).keypress(function(e){
        if (e.which == 13){
            $("#submit").click();
        }
    });

    //every 2 teconds the box is refreshed
    window.setInterval(function(){
        let messages = $.post('server.php',{action:'refresh'}).done(function( data ) {
            let box = document.getElementById("chatbox");
            box.innerHTML = data;
            box.scrollTop = box.scrollHeight;
        });
        
      }, 2000);
});