let army = [{
    Name:"Claude Wallace",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad E",
    Rank:"First Lieutenant",
    Role:"Tank Commander",
    Description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/9/97/VC4_Claude_Wallace.png/revision/latest?cb=20171120060049"
},{
    Name:"Riley Miller",
    Side:"Edinburgh Army",
    Unit:"Federate Joint Ops",
    Rank:"Second Lieutenant",
    Role:"Artillery Advisor",
    Description:"Born in the Gallian city of Hafen, this brilliant inventor was assigned to Squad E after researching ragnite technology in the United States of Vinland. She appears to share some history with Claude, although the memories seem to be traumatic ones.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/2/29/VC4_Riley_Miller.png/revision/latest?cb=20171120060031"
},{
    Name:"Raz",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad E",
    Rank:"Sergeant",
    Role:"Fireteam Leader",
    Description:"Born in the Gallian city of Hafen, this foul-mouthed Darcsen worked his way up from the slums to become a capable soldier. Though foul-mouthed and reckless, his athleticism and combat prowess is top-notch... And according to him, he's invincible.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/8/81/VC4_Raz.png/revision/latest?cb=20171127130324"
},{
    Name:"Kai Schulen",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad E",
    Rank:"Sergeant Major",
    Role:"Fireteam Leader",
    Description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/7/75/VC4_Kai_Schulen.png/revision/latest?cb=20171127130310"
},{
    Name:"Minerva Victor",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad F",
    Rank:"First Lieutenant",
    Role:"Senior Commander",
    Description:"Born in the United Kingdom of Edinburgh to a noble family, this competitive perfectionist has authority over the 101st Division's squad leaders. She values honor and chivalry, though a bitter rivalry with Lt. Wallace sometimes compromises her lofty ideals.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/c/cf/VC4_Minerva_Victor.png/revision/latest/scale-to-width-down/187?cb=20180119030358"
},{
    Name:"Karen Stuart",
    Side:"Edinburgh Army",
    Unit:"Squad E",
    Rank:"Corporal",
    Role:"Combat EMT",
    Description:"Born as the eldest daughter of a large family, this unflappable field medic is an expert at administering first aid in the heat of battle. Although she had plans to attend medical school, she instead enlisted in her nation's military to support her growing household.",
    image:"https://vignette.wikia.nocookie.net/valkyria/images/d/d6/VC4_Karen_Stewart.png/revision/latest?cb=20180119025755"
},{
    Name:"Ragnarok",
    Side:"Edinburgh Army",
    Unit:"Squad E",
    Rank:"K-9 Unit",
    Role:"Mascot",
    Description:"Once a stray, this good good boy is lovingly referred to as \"Rags.\"As a K-9 unit, he's a brave and intelligent rescue dog who's always willing to lend a helping paw. When the going gets tough, the tough get ruff.",
    image:"https://static.giantbomb.com/uploads/scale_small/9/97089/2989047-valkyria-chronicles-4_2017_12-25-17_007.png"
},{
    Name:"Miles Arbeck",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad E",
    Rank:"Sergeant",
    Role:"Tank Operator",
    Description:"Born in the United Kingdom of Edinburgh, this excitable driver was Claude Wallace's partner in tank training, and was delighted to be assigned to Squad E. He's taken up photography as a hobby, and is constantly taking snapshots whenever on standby.",
    image:"https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRtppM5eXUbxrG9uhgqM8IiUJ8NDMHN7k2Hyf_rbp-Ley7wrl7q"
},{
    Name:"Dan Bentley",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad E",
    Rank:"Private First Class",
    Role:"APC Operator",
    Description:"Born in the Gallian city of Hafen, this promising young squad leader is keenly attuned to climate changes and weather fluctuations. Ever since graduating as valedictorian from the Royal Military Academy, his talent and determination have been an inspiration to his subordinates.",
    image:"https://static.zerochan.net/Dan.Bentley.full.2271082.jpg"
},{
    Name:"Ronald Albee",
    Side:"Edinburgh Army",
    Unit:"Ranger Corps, Squad F",
    Rank:"Second Lieutenant",
    Role:"Tank Operator",
    Description:"Born in the United Kingdom of Edinburgh, this stern driver was Minerva Victor's underclassman at the military academy. Upon being assigned to Squad F, he swore an oath of fealty to Lt. Victor, and takes great satisfaction in upholding her chivalric code.",
    image:"https://static.zerochan.net/Ronald.Albee.full.2271083.jpg"
}];

//gets all important elements from the DOM and sets some global variables
let desc = document.querySelector("#roster");
let squad = document.querySelector("#squad");
let info = document.querySelector("#profile");
let pic = document.querySelector("#pic");
let cSquad = new Array();
let maxSquad = 5;

//populates sidebar
army.forEach(element => {
    desc.innerHTML += '<div class="roster-item" onClick="toggleMe(\''+element.Name+'\')" onmouseover="showMe(\''+element.Name+'\')" onmouseout="clearInfo()">'+element.Name+'</div>';
});

//updates the squad section
function updateRoster(){
    squad.innerHTML = "";
    cSquad.forEach(element => {
        squad.innerHTML += '<div class="squad-item" onClick="toggleMe(\''+element+'\')" onmouseover="showMe(\''+element+'\')" onmouseout="clearInfo()">'+element+'</div>';
    });
}

let names = document.querySelectorAll(".roster-item");

//toggles class and name in squad
function toggleMe(name){
    let found = false;
    
    for(let i = 0; i < cSquad.length; i++) {
        if (cSquad[i] == name) {
            found = true;
            cSquad.splice(i,1);
            for(let j = 0; i < names.length; j++) {
                if (names[j].innerHTML == name) {
                    names[j].classList.remove("selected");
                break;
                }
            }
            break;
        }
    }
    if(cSquad.length < maxSquad){
        if(!found){
            cSquad.push(name);
            console.log(cSquad);
            for(let k = 0; k < names.length; k++) {
                if (names[k].innerHTML == name) {
                    names[k].classList.add("selected");
                break;
                }
            }
        }
        updateRoster();
    }
}

//populates the info and image area when mouse is over a name
function showMe(name){
    console.log(name);
    info.innerHTML = "";
    let m = 0;
    for(m; m < army.length; m++) {
        if (army[m].Name == name) {
            for (var property in army[m]) {
                info.innerHTML +=  '<div class="info-item">' + property + ": "+army[m][property]+"</div>";
                if(property == "image"){
                    pic.innerHTML =  '<img src="'+army[m][property]+'" alt="'+army[m][property]+'">';  
                }
            }
        break;
        }
    }
}

//clears info and image panels
function clearInfo(){
    info.innerHTML = "";
    pic.innerHTML = "";
}